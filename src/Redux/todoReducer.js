import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    todoList: [{nombre: 'crear proyecto', id: 0}],
    lastIndex: 0,
}

export const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        add: (state, action) => {
            state.lastIndex ++;
            const obj = {
                nombre: action.payload,
                id: state.lastIndex,
            }
            const lista = [...state.todoList];
            lista.push(obj);
            state.todoList = lista;
        },
        remove: (state, action) => {
            const newList = state.todoList.filter(todo => todo.id !== action.payload)
            state.todoList = [...newList];
        }
    }
});

export const {add, remove} = todoSlice.actions;
export default todoSlice.reducer;