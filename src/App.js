import logo from './logo.svg';
import './App.css';
import {TodoList} from "./Components/TodoList";
import {New} from "./Components/New";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
        <New />
      <TodoList />
    </div>
  );
}

export default App;
