import {fireEvent, render, screen, waitFor, within} from '@testing-library/react';
import App from './App';
import {store} from "./store";
import {Provider} from "react-redux";
import userEvent from "@testing-library/user-event";

test('renders learn react link', async () => {
  render(<Provider store={store}>
    <App />
  </Provider>);

  const todoValue = 'mi nuevo valor';
  const input = await screen.findByPlaceholderText('nombre');
  const boton = await screen.findByText('Add');

  await fireEvent.change(input, {target: {value: todoValue}})
  await userEvent.click(boton);

  const listComponent = screen.getByTitle('lista');
  expect(within(listComponent).getByText(todoValue)).toBeInTheDocument();
});
