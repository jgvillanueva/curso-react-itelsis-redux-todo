import {useState} from "react";
import {useDispatch} from "react-redux";
import {add} from "../Redux/todoReducer";

export function New() {
    const [nuevoValor, setNuevoValor] = useState('');
    const dispatch = useDispatch();
    const handleChange = (event) => {
        setNuevoValor(event.target.value);
    }
    const handleClick = (event) => {
        event.preventDefault();
        dispatch(add(nuevoValor));
    }

    return (
        <div className="new">
            <form>
                <input value={nuevoValor} onChange={handleChange} placeholder="nombre"/>
                <button onClick={handleClick}>Add</button>
            </form>
        </div>
    )
}