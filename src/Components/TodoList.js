import {useDispatch, useSelector} from "react-redux";
import {add, remove} from "../Redux/todoReducer";

export function TodoList() {
    const todoList = useSelector((state) => {
        return state.todos.todoList;
    });
    const dispatch = useDispatch();

    const handleClick = (id) => {
        dispatch(remove(id));
    }

    const getTodoItems = () => {
        return todoList.map((todo) => {
            return <li key={todo.id}>
                {todo.nombre}
                <button onClick={() => {
                    handleClick(todo.id)
                }} >Remove</button>
            </li>
        })
    }

    return (
        <div className="todo-list" title="lista">
            {getTodoItems()}
        </div>
    )
}